package com.zhiche.wms.admin.test.surpports;

import com.zhiche.wms.WebApplication;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HnInterfaceControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }


    @Test   // 测试get请求
    public void testGetMapping() throws Exception {
        // 填写mapping 路径
        MvcResult mvcResult = mockMvc
                .perform(MockMvcRequestBuilders.get("/xxxxx/xxxxx")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .header("Authorization", "Bearer XXX")
                        .param("key", "value").param("key", "value")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        Assert.assertThat(mvcResult.getResponse().getContentAsString(), Matchers.containsString("0"));
    }

    @Test  // 测试post请求
    public void testPostMapping() throws Exception {
        // 填写mapping 路径
        MultiValueMap<String, String> valueMap = new LinkedMultiValueMap<>();
        // 构造参数 key 为接收对象属性
        valueMap.add("key", "VIN_WHS048");
        valueMap.add("houseId", "10001");
        mockMvc.perform(MockMvcRequestBuilders.post("/WmsInterfaceForHN/inboundCar")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJnbXRfY3JlYXRlIjoiMTUzNDk4Njc5NDM5MyIsImFjY291bnRJZCI6NTkxLCJ1c2VyX25hbWUiOiIxODc5MjY5NDI5NCIsInNjb3BlIjpbImFsbCJdLCJyb2xlcyI6W3siYXV0aG9yaXR5IjoiUk9MRV9OT1JNQUwifV0sInRlbmFudElkIjoxLCJjb3JwTmFtZSI6IuS4reiBlOeJqea1ge-8iOS4reWbve-8ieaciemZkOWFrOWPuCIsImV4cCI6MTUzNDk5Mzk5NCwiYXV0aG9yaXRpZXMiOlsiUk9MRV9OT1JNQUwiXSwianRpIjoiNzA1ZTgwYjAtNDAwOS00OGY1LTk3MTAtMjEyZDBjNTlhOWFjIiwiY2xpZW50X2lkIjoiZGV2b3BzIiwidXNlcm5hbWUiOiIxODc5MjY5NDI5NCJ9.xXbxb4iD-7Gy2pcTb2T_th4e5jQYY4gxROBCfzaKhhE")
                .params(valueMap)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("0")));
    }

}
