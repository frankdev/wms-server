package com.zhiche.wms.admin.controller.inbound;

import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.inbound.InboundInspectDTO;
import com.zhiche.wms.service.inbound.IInboundInspectLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 入库质检
 */
@RestController
@RequestMapping("inboundInspect")
public class InboundInspectController {
    @Autowired
    private IInboundInspectLineService inspectLineService;

    @PostMapping("/listInspect")
    public RestfulResponse<Page<InboundInspectDTO>> listInspect(@RequestBody Page<InboundInspectDTO> page) {
        Page<InboundInspectDTO> data = inspectLineService.listInspectPage(page);
        return new RestfulResponse<>(0, "查询成功", data);
    }

    @PostMapping("/exportInspect")
    public RestfulResponse<List<InboundInspectDTO>> exportInpsect(@RequestBody Map<String,Object> condition) {
        List<InboundInspectDTO> data = inspectLineService.queryExportInspect(condition);
        return new RestfulResponse<>(0, "查询成功", data);
    }

}
