package com.zhiche.wms.domain.mapper.inbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.inbound.InboundInspectHeader;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * <p>
 * 入库质检单头 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-22
 */
public interface InboundInspectHeaderMapper extends BaseMapper<InboundInspectHeader> {
    Integer updateStatus(@Param("id")Long id, @Param("houseId")Long houseId, @Param("status")Integer status, @Param("qualifiedSum")BigDecimal qualifiedSum, @Param("damagedSum")BigDecimal damagedSum);
}
