package com.zhiche.wms.domain.model.base;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 业务单据号
 * </p>
 *
 * @author qichao
 * @since 2018-06-07
 */
@TableName("w_business_doc_number")
public class BusinessDocNumber extends Model<BusinessDocNumber> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 前缀
     */
    private String prefix;
    /**
     * 当前值
     */
    @TableField("current_value")
    private Integer currentValue;
    /**
     * 下一值
     */
    @TableField("next_value")
    private Integer nextValue;
//    /**
//     * 创建时间
//     */
//	@TableField("gmt_create")
//	private Date gmtCreate;
//    /**
//     * 修改时间
//     */
//	@TableField("gmt_modified")
//	private Date gmtModified;
    /**
     * 状态(1:正常,0:失效)
     */
    private Integer status;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Integer getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(Integer currentValue) {
        this.currentValue = currentValue;
    }

    public Integer getNextValue() {
        return nextValue;
    }

    public void setNextValue(Integer nextValue) {
        this.nextValue = nextValue;
    }

//	public Date getGmtCreate() {
//		return gmtCreate;
//	}
//
//	public void setGmtCreate(Date gmtCreate) {
//		this.gmtCreate = gmtCreate;
//	}
//
//	public Date getGmtModified() {
//		return gmtModified;
//	}
//
//	public void setGmtModified(Date gmtModified) {
//		this.gmtModified = gmtModified;
//	}

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BusinessDocNumber{" +
                ", id=" + id +
                ", prefix=" + prefix +
                ", currentValue=" + currentValue +
                ", nextValue=" + nextValue +
//			", gmtCreate=" + gmtCreate +
//			", gmtModified=" + gmtModified +
                ", status=" + status +
                "}";
    }
}
