package com.zhiche.wms.domain.mapper.stock;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.stock.StockDetail;

/**
 * <p>
 * 库存明细账 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface StockDetailMapper extends BaseMapper<StockDetail> {

}
