package com.zhiche.wms.domain.model.outbound;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 出库备料单头
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-27
 */
@TableName("w_outbound_prepare_header")
public class OutboundPrepareHeader extends Model<OutboundPrepareHeader> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 出库通知单头键
     */
    @TableField("notice_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long noticeId;
    /**
     * 备料单号
     */
    @TableField("prepare_no")
    private String prepareNo;
    /**
     * 单据时间
     */
    @TableField("order_time")
    private Date orderTime;
    /**
     * 计划时间
     */
    @TableField("plan_time")
    private Date planTime;
    /**
     * 开始时间
     */
    @TableField("start_time")
    private Date startTime;
    /**
     * 完成时间
     */
    @TableField("finish_time")
    private Date finishTime;
    /**
     * 供应商
     */
    @TableField("carrier_id")
    private String carrierId;
    /**
     * 供应商名称
     */
    @TableField("carrier_name")
    private String carrierName;
    /**
     * 运输方式
     */
    @TableField("transport_method")
    private String transportMethod;
    /**
     * 车船号
     */
    @TableField("plate_number")
    private String plateNumber;
    /**
     * 司机姓名
     */
    @TableField("driver_name")
    private String driverName;
    /**
     * 司机联系方式
     */
    @TableField("driver_phone")
    private String driverPhone;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 计划备料数量
     */
    @TableField("plan_sum_qty")
    private BigDecimal planSumQty;
    /**
     * 实际备料数量
     */
    @TableField("actual_sum_qty")
    private BigDecimal actualSumQty;
    /**
     * 明细行数
     */
    @TableField("line_count")
    private Integer lineCount;
    /**
     * 状态(10:备料计划,20:开始执行,30:部分备料,40:全部完成)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    @TableField("loading_area")
    private String loadingArea;

    public String getLoadingArea() {
        return loadingArea;
    }

    public void setLoadingArea(String loadingArea) {
        this.loadingArea = loadingArea;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Long noticeId) {
        this.noticeId = noticeId;
    }

    public String getPrepareNo() {
        return prepareNo;
    }

    public void setPrepareNo(String prepareNo) {
        this.prepareNo = prepareNo;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getPlanTime() {
        return planTime;
    }

    public void setPlanTime(Date planTime) {
        this.planTime = planTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getTransportMethod() {
        return transportMethod;
    }

    public void setTransportMethod(String transportMethod) {
        this.transportMethod = transportMethod;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getPlanSumQty() {
        return planSumQty;
    }

    public void setPlanSumQty(BigDecimal planSumQty) {
        this.planSumQty = planSumQty;
    }

    public BigDecimal getActualSumQty() {
        return actualSumQty;
    }

    public void setActualSumQty(BigDecimal actualSumQty) {
        this.actualSumQty = actualSumQty;
    }

    public Integer getLineCount() {
        return lineCount;
    }

    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OutboundPrepareHeader{" +
                ", id=" + id +
                ", noticeId=" + noticeId +
                ", prepareNo=" + prepareNo +
                ", orderTime=" + orderTime +
                ", planTime=" + planTime +
                ", startTime=" + startTime +
                ", finishTime=" + finishTime +
                ", carrierId=" + carrierId +
                ", carrierName=" + carrierName +
                ", transportMethod=" + transportMethod +
                ", plateNumber=" + plateNumber +
                ", driverName=" + driverName +
                ", driverPhone=" + driverPhone +
                ", uom=" + uom +
                ", planSumQty=" + planSumQty +
                ", actualSumQty=" + actualSumQty +
                ", lineCount=" + lineCount +
                ", status=" + status +
                ", remarks=" + remarks +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
