package com.zhiche.wms.domain.mapper.log;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.log.ItfImplogLine;

/**
 * <p>
 * 仓储接口导入明细 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
public interface ItfImplogLineMapper extends BaseMapper<ItfImplogLine> {

}
