package com.zhiche.wms.domain.mapper.opbaas;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.zhiche.wms.domain.model.opbaas.ComponentConfig;
import com.zhiche.wms.dto.opbaas.resultdto.MissingRegidterDetailDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 缺件配置配置 Mapper 接口
 * </p>
 *
 * @author hxh
 * @since 2018-08-07
 */
public interface ComponentConfigMapper extends BaseMapper<ComponentConfig> {

    List<MissingRegidterDetailDTO> queryComponent(@Param("ew") Wrapper<MissingRegidterDetailDTO> ew);
}
