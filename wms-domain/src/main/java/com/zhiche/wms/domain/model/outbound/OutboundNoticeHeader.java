package com.zhiche.wms.domain.model.outbound;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 出库通知单头
 * </p>
 *
 * @author qichao
 * @since 2018-06-13
 */
@TableName("w_outbound_notice_header")
public class OutboundNoticeHeader extends Model<OutboundNoticeHeader> {

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<OutboundNoticeLine> outboundNoticeLineList;

    /**
     * id
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 发货仓库
     */
    @JsonSerialize(using=ToStringSerializer.class)
    @TableField("store_house_id")
    private Long storeHouseId;
    /**
     * 入库通知单号
     */
    @TableField("notice_no")
    private String noticeNo;
    /**
     * 货主
     */
    @TableField("owner_id")
    private String ownerId;
    /**
     * 货主单号
     */
    @TableField("owner_order_no")
    private String ownerOrderNo;
    /**
     * 下单日期
     */
    @TableField("order_date")
    private Date orderDate;
    /**
     * 供应商
     */
    @TableField("carrier_id")
    private String carrierId;
    /**
     * 供应商名称
     */
    @TableField("carrier_name")
    private String carrierName;
    /**
     * 运输方式
     */
    @TableField("transport_method")
    private String transportMethod;
    /**
     * 车船号
     */
    @TableField("plate_number")
    private String plateNumber;
    /**
     * 司机姓名
     */
    @TableField("driver_name")
    private String driverName;
    /**
     * 司机联系方式
     */
    @TableField("driver_phone")
    private String driverPhone;
    /**
     * 预计到货日期
     */
    @TableField("expect_ship_date_lower")
    private Date expectShipDateLower;
    /**
     * 预计收货时间上限
     */
    @TableField("expect_ship_date_upper")
    private Date expectShipDateUpper;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 预计入库数量
     */
    @TableField("expect_sum_qty")
    private BigDecimal expectSumQty;
    /**
     * 已入库数量
     */
    @TableField("outbound_sum_qty")
    private BigDecimal outboundSumQty;
    /**
     * 明细行数
     */
    @TableField("line_count")
    private Integer lineCount;
    /**
     * 资料生成方式(10:手工创建,20:oms系统触发,30:tms系统触发,40:君马系统触发)
     */
    @TableField("gen_method")
    private String genMethod;
    /**
     * 状态(10:未出库,20:部分出库,30:全部出库,40:关闭出库,50:取消)
     */
    private String status;
    /**
     * 日志头ID
     */
    @TableField("log_header_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long logHeaderId;
    /**
     * 来源唯一键
     */
    @TableField("source_key")
    private String sourceKey;
    /**
     * 来源单据号
     */
    @TableField("source_no")
    private String sourceNo;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getNoticeNo() {
        return noticeNo;
    }

    public void setNoticeNo(String noticeNo) {
        this.noticeNo = noticeNo;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getTransportMethod() {
        return transportMethod;
    }

    public void setTransportMethod(String transportMethod) {
        this.transportMethod = transportMethod;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public Date  getExpectShipDateLower() {
        return expectShipDateLower;
    }

    public void setExpectShipDateLower(Date expectShipDateLower) {
        this.expectShipDateLower = expectShipDateLower;
    }

    public Date getExpectShipDateUpper() {
        return expectShipDateUpper;
    }

    public void setExpectShipDateUpper(Date expectShipDateUpper) {
        this.expectShipDateUpper = expectShipDateUpper;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getExpectSumQty() {
        return expectSumQty;
    }

    public void setExpectSumQty(BigDecimal expectSumQty) {
        this.expectSumQty = expectSumQty;
    }

    public BigDecimal getOutboundSumQty() {
        return outboundSumQty;
    }

    public void setOutboundSumQty(BigDecimal outboundSumQty) {
        this.outboundSumQty = outboundSumQty;
    }

    public Integer getLineCount() {
        return lineCount;
    }

    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }

    public String getGenMethod() {
        return genMethod;
    }

    public void setGenMethod(String genMethod) {
        this.genMethod = genMethod;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getLogHeaderId() {
        return logHeaderId;
    }

    public void setLogHeaderId(Long logHeaderId) {
        this.logHeaderId = logHeaderId;
    }

    public String getSourceKey() {
        return sourceKey;
    }

    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    public String getSourceNo() {
        return sourceNo;
    }

    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OutboundNoticeHeader{" +
                ", id=" + id +
                ", storeHouseId=" + storeHouseId +
                ", noticeNo=" + noticeNo +
                ", ownerId=" + ownerId +
                ", ownerOrderNo=" + ownerOrderNo +
                ", orderDate=" + orderDate +
                ", carrierId=" + carrierId +
                ", carrierName=" + carrierName +
                ", transportMethod=" + transportMethod +
                ", plateNumber=" + plateNumber +
                ", driverName=" + driverName +
                ", driverPhone=" + driverPhone +
                ", expectShipDateLower=" + expectShipDateLower +
                ", expectShipDateUpper=" + expectShipDateUpper +
                ", uom=" + uom +
                ", expectSumQty=" + expectSumQty +
                ", outboundSumQty=" + outboundSumQty +
                ", lineCount=" + lineCount +
                ", genMethod=" + genMethod +
                ", status=" + status +
                ", logHeaderId=" + logHeaderId +
                ", sourceKey=" + sourceKey +
                ", sourceNo=" + sourceNo +
                ", remarks=" + remarks +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }

    public List<OutboundNoticeLine> getOutboundNoticeLineList() {
        return outboundNoticeLineList;
    }

    public void setOutboundNoticeLineList(List<OutboundNoticeLine> outboundNoticeLineList) {
        this.outboundNoticeLineList = outboundNoticeLineList;
    }

    public void addOutboundNoticeLine(OutboundNoticeLine outboundNoticeLine) {
        if (Objects.isNull(outboundNoticeLineList)) {
            outboundNoticeLineList = new ArrayList<>();
        }
        outboundNoticeLineList.add(outboundNoticeLine);
    }
}
