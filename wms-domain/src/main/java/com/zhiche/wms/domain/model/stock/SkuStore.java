package com.zhiche.wms.domain.model.stock;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 停放位置信息
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-10-19
 */
@TableName("w_sku_store")
public class SkuStore extends Model<SkuStore> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 库存单元id
     */
	@TableField("sku_id")
	private String skuId;
    /**
     * 仓库id
     */
	@TableField("store_house_id")
	private Long storeHouseId;
    /**
     * 实车停放位置
     */
	@TableField("store_detail")
	private String storeDetail;
    /**
     * 状态值
     */
	@TableField("vin")
	private String vin;
    /**
     * 创建人
     */
	@TableField("user_create")
	private String userCreate;
    /**
     * 创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;

	@TableField("remark")
    private String remark;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}

	public Long getStoreHouseId() {
		return storeHouseId;
	}

	public void setStoreHouseId(Long storeHouseId) {
		this.storeHouseId = storeHouseId;
	}

	public String getStoreDetail() {
		return storeDetail;
	}

	public void setStoreDetail(String storeDetail) {
		this.storeDetail = storeDetail;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUserCreate() {
		return userCreate;
	}

	public void setUserCreate(String userCreate) {
		this.userCreate = userCreate;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

    @Override
    public String toString() {
        return "SkuStore{" +
                "id=" + id +
                ", skuId='" + skuId + '\'' +
                ", storeHouseId=" + storeHouseId +
                ", storeDetail='" + storeDetail + '\'' +
                ", vin='" + vin + '\'' +
                ", userCreate='" + userCreate + '\'' +
                ", gmtCreate=" + gmtCreate +
                ", remark='" + remark + '\'' +
                '}';
    }
}
