package com.zhiche.wms.domain.model.inbound;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 入库质检单头
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-22
 */
@TableName("w_inbound_inspect_header")
public class InboundInspectHeader extends Model<InboundInspectHeader> {
//	@TableField(exist = false)
//	private List<InboundInspectLine> inboundInspectLineList;
//
//	public List<InboundInspectLine> getInboundInspectLineList() {
//		return inboundInspectLineList;
//	}
//
//	public void setInboundInspectLineList(List<InboundInspectLine> inboundInspectLineList) {
//		this.inboundInspectLineList = inboundInspectLineList;
//	}
//
//	public void addInboundInspectLineList(InboundInspectLine inboundInspectLine){
//		if(Objects.isNull(inboundInspectLineList)){
//			inboundInspectLineList = new ArrayList<>();
//		}
//		inboundInspectLineList.add(inboundInspectLine);
//	}

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 入库通知单头键
     */
    @TableField("notice_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long noticeId;
    /**
     * 质检单号
     */
    @TableField("inspect_no")
    private String inspectNo;
    /**
     * 质检日期
     */
    @TableField("order_date")
    private Date orderDate;
    /**
     * 质检员
     */
    private String inspector;
    /**
     * 供应商
     */
    @TableField("carrier_id")
    private String carrierId;
    /**
     * 供应商名称
     */
    @TableField("carrier_name")
    private String carrierName;
    /**
     * 运输方式
     */
    @TableField("transport_method")
    private String transportMethod;
    /**
     * 车船号
     */
    @TableField("plate_number")
    private String plateNumber;
    /**
     * 司机姓名
     */
    @TableField("driver_name")
    private String driverName;
    /**
     * 司机联系方式
     */
    @TableField("driver_phone")
    private String driverPhone;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 收货数量
     */
    @TableField("recv_sum_qty")
    private BigDecimal recvSumQty;
    /**
     * 合格数量
     */
    @TableField("qualified_sum_qty")
    private BigDecimal qualifiedSumQty;
    /**
     * 破损数量
     */
    @TableField("damaged_sum_qty")
    private BigDecimal damagedSumQty;
    /**
     * 明细行数
     */
    @TableField("line_count")
    private Integer lineCount;
    /**
     * 是否完成(1:是;0:否)
     */
    @TableField("is_finish")
    private Integer isFinish;
    /**
     * 状态(10:全部合格,20:部分合格,30:全部质损)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Long noticeId) {
        this.noticeId = noticeId;
    }

    public String getInspectNo() {
        return inspectNo;
    }

    public void setInspectNo(String inspectNo) {
        this.inspectNo = inspectNo;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getTransportMethod() {
        return transportMethod;
    }

    public void setTransportMethod(String transportMethod) {
        this.transportMethod = transportMethod;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getRecvSumQty() {
        return recvSumQty;
    }

    public void setRecvSumQty(BigDecimal recvSumQty) {
        this.recvSumQty = recvSumQty;
    }

    public BigDecimal getQualifiedSumQty() {
        return qualifiedSumQty;
    }

    public void setQualifiedSumQty(BigDecimal qualifiedSumQty) {
        this.qualifiedSumQty = qualifiedSumQty;
    }

    public BigDecimal getDamagedSumQty() {
        return damagedSumQty;
    }

    public void setDamagedSumQty(BigDecimal damagedSumQty) {
        this.damagedSumQty = damagedSumQty;
    }

    public Integer getLineCount() {
        return lineCount;
    }

    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }

    public Integer getIsFinish() {
        return isFinish;
    }

    public void setIsFinish(Integer isFinish) {
        this.isFinish = isFinish;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "InboundInspectHeader{" +
                ", id=" + id +
                ", noticeId=" + noticeId +
                ", inspectNo=" + inspectNo +
                ", orderDate=" + orderDate +
                ", inspector=" + inspector +
                ", carrierId=" + carrierId +
                ", carrierName=" + carrierName +
                ", transportMethod=" + transportMethod +
                ", plateNumber=" + plateNumber +
                ", driverName=" + driverName +
                ", driverPhone=" + driverPhone +
                ", uom=" + uom +
                ", recvSumQty=" + recvSumQty +
                ", qualifiedSumQty=" + qualifiedSumQty +
                ", damagedSumQty=" + damagedSumQty +
                ", lineCount=" + lineCount +
                ", isFinish=" + isFinish +
                ", status=" + status +
                ", remarks=" + remarks +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
