package com.zhiche.wms.dto.outbound;

import com.zhiche.wms.domain.model.outbound.OutboundPrepareHeader;

import java.io.Serializable;

public class OutboundPrepareVehicleDTO extends OutboundPrepareHeader implements Serializable {
    private String serviceProviderName;
    private String sourceKey;

    public String getServiceProviderName() {
        return serviceProviderName;
    }

    public void setServiceProviderName(String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }

    public String getSourceKey() {
        return sourceKey;
    }

    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey;
    }
}
