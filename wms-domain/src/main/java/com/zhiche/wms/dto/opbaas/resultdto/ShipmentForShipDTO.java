package com.zhiche.wms.dto.opbaas.resultdto;

import com.zhiche.wms.domain.model.otm.OtmShipment;

import java.io.Serializable;

public class ShipmentForShipDTO extends OtmShipment implements Serializable {
    private Integer shipCount;
    private Integer handoverCount;

    public Integer getShipCount() {
        return shipCount;
    }

    public void setShipCount(Integer shipCount) {
        this.shipCount = shipCount;
    }

    public Integer getHandoverCount() {
        return handoverCount;
    }

    public void setHandoverCount(Integer handoverCount) {
        this.handoverCount = handoverCount;
    }
}
