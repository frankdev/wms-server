package com.zhiche.wms.dto.opbaas.resultdto;

import com.zhiche.wms.domain.model.opbaas.ExceptionRegister;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegisterPicture;

import java.io.Serializable;
import java.util.List;

public class PictureWithExcpDescDTO extends ExceptionRegister implements Serializable {
    private List<ExceptionRegisterPicture> pictures;

    public List<ExceptionRegisterPicture> getPictures() {
        return pictures;
    }

    public void setPictures(List<ExceptionRegisterPicture> pictures) {
        this.pictures = pictures;
    }
}
