package com.zhiche.wms.dto.opbaas.resultdto;

import java.io.Serializable;

public class TaskOrderInfoResultDTO implements Serializable {

    private String orderReleaseGid;
    private String sourceLocationGid;
    private String sourceLocationName;
    private String destLocationGid;
    private String destLocationName;
    private String attribute15;
    private String attribute1;
    private String attribute7;
    private String orderReleaseName;
    private String taskId;
    private String type;
    private String status;

    public String getOrderReleaseGid() {
        return orderReleaseGid;
    }

    public void setOrderReleaseGid(String orderReleaseGid) {
        this.orderReleaseGid = orderReleaseGid;
    }

    public String getSourceLocationGid() {
        return sourceLocationGid;
    }

    public void setSourceLocationGid(String sourceLocationGid) {
        this.sourceLocationGid = sourceLocationGid;
    }

    public String getSourceLocationName() {
        return sourceLocationName;
    }

    public void setSourceLocationName(String sourceLocationName) {
        this.sourceLocationName = sourceLocationName;
    }

    public String getDestLocationGid() {
        return destLocationGid;
    }

    public void setDestLocationGid(String destLocationGid) {
        this.destLocationGid = destLocationGid;
    }

    public String getDestLocationName() {
        return destLocationName;
    }

    public void setDestLocationName(String destLocationName) {
        this.destLocationName = destLocationName;
    }

    public String getAttribute15() {
        return attribute15;
    }

    public void setAttribute15(String attribute15) {
        this.attribute15 = attribute15;
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

    public String getAttribute7() {
        return attribute7;
    }

    public void setAttribute7(String attribute7) {
        this.attribute7 = attribute7;
    }

    public String getOrderReleaseName() {
        return orderReleaseName;
    }

    public void setOrderReleaseName(String orderReleaseName) {
        this.orderReleaseName = orderReleaseName;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
