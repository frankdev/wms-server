package com.zhiche.wms.dto.sys;

import com.zhiche.wms.domain.model.base.StorehouseNode;

import java.io.Serializable;

public class StorehouseNodeDTO extends StorehouseNode implements Serializable {
    private String nodeCode;
    private String nodeName;
    private String nodeStatus;
    private String nodeFlag;

    public String getNodeFlag() {
        return nodeFlag;
    }

    public void setNodeFlag(String nodeFlag) {
        this.nodeFlag = nodeFlag;
    }

    public String getNodeCode() {
        return nodeCode;
    }

    public void setNodeCode(String nodeCode) {
        this.nodeCode = nodeCode;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getNodeStatus() {
        return nodeStatus;
    }

    public void setNodeStatus(String nodeStatus) {
        this.nodeStatus = nodeStatus;
    }
}
