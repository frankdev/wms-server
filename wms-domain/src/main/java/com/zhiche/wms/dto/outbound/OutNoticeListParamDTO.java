package com.zhiche.wms.dto.outbound;

import com.zhiche.wms.dto.base.PageVo;

import java.io.Serializable;

public class OutNoticeListParamDTO extends PageVo implements Serializable {
    private String noticeHeadNo;
    private String houseId;
    private String ownerId;
    private String ownerOrderNo;
    private String status;

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getNoticeHeadNo() {
        return noticeHeadNo;
    }

    public void setNoticeHeadNo(String noticeHeadNo) {
        this.noticeHeadNo = noticeHeadNo;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
