package com.zhiche.wms.app.controller.opbaas;

import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.opbaas.paramdto.MissingRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExConfigurationDTO;
import com.zhiche.wms.dto.opbaas.resultdto.MissingRegidterDetailDTO;
import com.zhiche.wms.service.opbaas.IMissingComponentRegisterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 缺件登记 前端控制器
 * </p>
 *
 * @author hxh
 * @since 2018-08-07
 */
@Api(value = "missingComponentRegister-API", description = "缺件登记操作接口")
@RestController
@RequestMapping("/missingRegister")
public class MissingComponentRegisterController {

    @Autowired
    IMissingComponentRegisterService missingComponentRegisterService;

    @ApiOperation(value = "获取缺件登记界面信息", notes = "获取缺件登记界面信息")
    @PostMapping("/getMissingInfo")
    @Deprecated
    public RestfulResponse<List<MissingRegidterDetailDTO>> getMissingInfo(MissingRegisterParamDTO dto) {
        RestfulResponse<List<MissingRegidterDetailDTO>> restfulResponse = new RestfulResponse<>(0, "获取缺件信息成功", null);
        List<MissingRegidterDetailDTO> resultDTOList = missingComponentRegisterService.queryMissingInfo(dto);
        restfulResponse.setData(resultDTOList);
        return restfulResponse;
    }

    @ApiOperation(value = "缺件登记", notes = "缺件登记")
    @PostMapping("/updateMissingInfo")
    @Deprecated
    public RestfulResponse<List<MissingRegidterDetailDTO>> updateMissingInfo(MissingRegisterParamDTO dto) {
        RestfulResponse<List<MissingRegidterDetailDTO>> restfulResponse = new RestfulResponse<>(0, "登记成功", null);
        List<MissingRegidterDetailDTO> resultDTOList = missingComponentRegisterService.updateMissingInfo(dto);
        restfulResponse.setData(resultDTOList);
        return restfulResponse;
    }

}

