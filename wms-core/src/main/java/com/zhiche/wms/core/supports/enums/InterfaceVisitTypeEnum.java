package com.zhiche.wms.core.supports.enums;

public enum InterfaceVisitTypeEnum {
    CLICK_TYPE("CLICK","click_type方式请求"),
    SCAN_TYPE("SCAN","scan_type方式请求");

    private final String code;
    private final String detail;

    public String getCode() {
        return code;
    }

    public String getDetail() {
        return detail;
    }

    InterfaceVisitTypeEnum(String code, String detail) {
        this.code = code;
        this.detail = detail;
    }
}
