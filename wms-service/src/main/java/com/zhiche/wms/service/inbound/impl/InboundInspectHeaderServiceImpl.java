package com.zhiche.wms.service.inbound.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.inbound.InboundInspectHeaderMapper;
import com.zhiche.wms.domain.model.inbound.InboundInspectHeader;
import com.zhiche.wms.service.inbound.IInboundInspectHeaderService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 * 入库质检单头 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-22
 */
@Service
public class InboundInspectHeaderServiceImpl extends ServiceImpl<InboundInspectHeaderMapper, InboundInspectHeader> implements IInboundInspectHeaderService {
    public Integer updateStatus(Long id, Long houseId, Integer status, BigDecimal qualifiedSum, BigDecimal damagedSum){
        return this.baseMapper.updateStatus(id,houseId,status,qualifiedSum,damagedSum);
    }
}
