package com.zhiche.wms.service.constant;

/**
 * 移位单类型 Created by zhaoguixin on 2018/6/11.
 */
public class MovementType {

    /**
     * 入库移位
     */
    public static final String INBOUND_MOVE = "10";

    /**
     * 出库移位
     */
    public static final String OUTBOUND_MOVE = "20";

    /**
     * 调整移位
     */
    public static final String ADJUST_MOVE = "30";


    /**
     * 入位
     */
    public static final String LINE_INTO = "10";

    /**
     * 出位
     */
    public static final String LINE_OUT = "20";

    /**
     * 移位
     */
    public static final String LINE_MOVE = "30";


}
