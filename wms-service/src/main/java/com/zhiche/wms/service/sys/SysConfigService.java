package com.zhiche.wms.service.sys;


import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.sys.SysConfig;
import com.zhiche.wms.domain.model.sys.User;

import java.util.HashMap;

/**
 * <p>
 * WMS各功能节点配置 服务类
 * </p>
 *
 * @author zhangkun
 * @since 2018-11-30
 */
public interface SysConfigService extends IService<SysConfig> {

    /**
     * 分页查询配置
     */
    Page<SysConfig> selectUserPage(Page<SysConfig> page);

    /**
     * 任务时间间隔配置
     * @param params
     */
    void timeConfig (HashMap <String,Object> params);

    void updateSysConfig(SysConfig sysConfig);
}
