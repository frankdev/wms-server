package com.zhiche.wms.service.log.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.log.ItfImplogLineMapper;
import com.zhiche.wms.domain.model.log.ItfImplogLine;
import com.zhiche.wms.service.log.IItfImplogLineService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 仓储接口导入明细 服务实现类
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
@Service
public class ItfImplogLineServiceImpl extends ServiceImpl<ItfImplogLineMapper, ItfImplogLine> implements IItfImplogLineService {

}
