package com.zhiche.wms.service.stockadjust;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.stockadjust.StockAdjustHeader;

/**
 * <p>
 * 库存调整单头 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface IStockAdjustHeaderService extends IService<StockAdjustHeader> {

    /**
     * 创建库存调整
     * @param stockAdjustHeader
     * @return
     */
    boolean creatStockAdjust(StockAdjustHeader stockAdjustHeader);

    /**
     * 审核库存调整
     * @param stockAdjustId
     * @return
     */
    boolean auditStockAdjust(Long stockAdjustId);

    /**
     * 创建并审核库存调整
     * @param stockAdjustHeader
     * @return
     */
    boolean creatAndAuditStockAdjust(StockAdjustHeader stockAdjustHeader);

    /**
     * 撤销审核库存调整
     * @param stockAdjustId
     * @return
     */
    boolean cancelAuditStockAdjust(Long stockAdjustId);
}
