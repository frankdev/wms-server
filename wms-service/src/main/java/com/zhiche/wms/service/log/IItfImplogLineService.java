package com.zhiche.wms.service.log;


import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.log.ItfImplogLine;

/**
 * <p>
 * 仓储接口导入明细 服务类
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
public interface IItfImplogLineService extends IService<ItfImplogLine> {

}
