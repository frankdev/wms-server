package com.zhiche.wms.service.sys.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.enums.NodeOptionEnum;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.domain.mapper.opbaas.OpTaskMapper;
import com.zhiche.wms.domain.mapper.sys.SysConfigMapper;
import com.zhiche.wms.domain.model.opbaas.OpTask;
import com.zhiche.wms.domain.model.sys.SysConfig;
import com.zhiche.wms.domain.model.sys.User;
import com.zhiche.wms.service.sys.IUserService;
import com.zhiche.wms.service.sys.SysConfigService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * WMS各功能节点配置 服务实现类
 * </p>
 *
 * @author zhangkun
 * @since 2018-11-30
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements SysConfigService {
    @Autowired
    private OpTaskMapper opTaskMapper;
    @Autowired
    private IUserService userService;
    @Override
    public Page<SysConfig> selectUserPage(Page<SysConfig> page) {
        if (page == null) {
            throw new BaseException("参数不能为空");
        }
        Map<String, Object> cd = page.getCondition();
        EntityWrapper<SysConfig> ew = new EntityWrapper<>();
        if (cd != null) {
            if (Objects.nonNull(cd.get("releationId")) && StringUtils.isNotBlank(cd.get("releationId").toString())) {
                ew.eq("releation_id", cd.get("releationId").toString().trim());
            }
        }
        return this.selectPage(page,ew);
    }

    @Override
    public void timeConfig(HashMap<String, Object> params) {
        OpTask opTask = opTaskMapper.queryNewTask(params);

        if (Objects.nonNull(opTask)) {
            Wrapper<SysConfig> sysConfigWrapper = new EntityWrapper<>();
            sysConfigWrapper.eq("releation_id", params.get("pointId"))
                    .eq("config_type", "POINT")
                    .eq("config_level",TableStatusEnum.STATUS_2.getCode())
                    .eq("configed", TableStatusEnum.STATUS_1.getCode())
                    .eq("code", NodeOptionEnum.TIME_CONFIG.getCode());
            SysConfig sysConfig = this.selectOne(sysConfigWrapper);
            if (Objects.nonNull(sysConfig)) {
                Calendar instance = Calendar.getInstance();
                instance.setTime(new Date());
                instance.add(Calendar.MINUTE, -Integer.valueOf(sysConfig.getConfigDetail()));
                Date minusTime = instance.getTime();
                if (opTask.getStartTime().after(minusTime) ) {
                    throw new BaseException("操作频繁,请稍后再试!");
                }
                if (Objects.nonNull(opTask.getFinishTime())
                        && opTask.getFinishTime().after(minusTime)){
                    throw new BaseException("操作频繁,请稍后再试!");
                }
            }
        }
    }

    @Override
    public void updateSysConfig(SysConfig sysConfig) {
        if (Objects.isNull(sysConfig) || Objects.isNull(sysConfig.getId())){
            throw  new BaseException("参数不能为空");
        }
        User loginUser = userService.getLoginUser();
        if (loginUser == null) {
            throw new BaseException("未查询到登录用户");
        }
        this.updateById(sysConfig);
    }
}
