package com.zhiche.wms.service.sys;

import com.zhiche.wms.domain.model.sys.UserDeliveryPoint;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户发车点权限表 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
public interface IUserDeliveryPointService extends IService<UserDeliveryPoint> {

}
